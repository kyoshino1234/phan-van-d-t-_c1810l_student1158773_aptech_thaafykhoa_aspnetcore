﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Data
{
    public class Question
    {
        [Key]
        public int Id { get; set; }
        public string NameQuestion { get; set; }
        public int IdQType { get; set; }
        [ForeignKey("IdQType")]
        public QuestionType QuestionType { get; set; }

        public int Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
