﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Data
{
    public class OrderDetail
    {
        [Key]
        public int IdOrderDetail { get; set; }
        public int IdProduct { get; set; }

        [ForeignKey("IdProduct")]
        public Product Product { get; set; }
        public int Quantity { get; set; }

        public int Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
