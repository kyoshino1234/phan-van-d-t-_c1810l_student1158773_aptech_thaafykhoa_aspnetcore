﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Data
{
    public class Product
    {
        [Key]
        public int IdProduct { get; set; }
        public string NameProduct { get; set; }
        public double Price { get; set; }

        public int Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
