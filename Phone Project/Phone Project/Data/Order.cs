﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Data
{
    public class Order
    {
        [Key]
        public int IdOrder { get; set; }
        public int IdOrderDetail { get; set; }

        [ForeignKey("IdOrderDetail")]
        public OrderDetail OrderDetail { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Double Total { get; set; }

        public int Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
