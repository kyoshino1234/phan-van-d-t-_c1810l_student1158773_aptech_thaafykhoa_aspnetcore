﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Models
{
    public class LeaveTypeVM
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Display(Name ="Date Created")]
        public DateTime DateCreated { get; set; }
    }

   /* public class CreateLeaveTypeVM
    {
        [Required]
        public string name { get; set; }
    }*/
}
