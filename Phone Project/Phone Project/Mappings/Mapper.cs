﻿
using AutoMapper;
using Phone_Project.Data;
using Phone_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Mappings
{
    public class Mapper: Profile
    {
       
        public Mapper()
        {
            CreateMap<LeaveType, LeaveTypeVM>().ReverseMap();
            //CreateMap<LeaveType, CreateLeaveTypeVM>().ReverseMap();
            CreateMap<LeaveHistory, LeaveHistoryVM>().ReverseMap();
            CreateMap<LeaveAllocation, LeaveAllocationVM>().ReverseMap();
            CreateMap<Employee, EmployeeVM>().ReverseMap();
        }
       
    }
}
