﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phone_Project.Contracts;
using Phone_Project.Data;

namespace Phone_Project.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class OrderDetailController : Controller
    {
        private readonly IOrderDetailRepository _repo;

        public OrderDetailController(IOrderDetailRepository repo)
        {
            this._repo = repo;
        }
        // GET: OrderDetailController
     
        public ActionResult Index()
        {
            List<OrderDetail> model = _repo.FindAll().ToList();
            return View(model);
        }

        // GET: OrderDetailController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: OrderDetailController/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: OrderDetailController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderDetail model)
        {
            try
            {

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderDetailController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: OrderDetailController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderDetailController/Delete/5
        [Area("Admin")]
        public ActionResult Delete(int id)
        {
           
            try
            {
                if (!_repo.isExists(id))
                {
                    return NotFound();
                }
                var orderDetail = _repo.FindById(id);
                var isSuccess = _repo.Delete(orderDetail);
                if (!isSuccess)
                {
                    ModelState.AddModelError("", "Failt Delete OrdeDetail");
                    return View();
                }
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // POST: OrderDetailController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, OrderDetail orderDetail)
        {
            try
            {
                if (!_repo.isExists(id))
                {
                    return NotFound();
                }
                else
                {
                    var item = _repo.FindById(id);
                    var isSuccess = _repo.Delete(item);
                    if (!isSuccess)
                    {
                        ModelState.AddModelError("", "Fail Create LeaveType");
                        return View();
                    }
                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return View();
            }
        }
    }
}
