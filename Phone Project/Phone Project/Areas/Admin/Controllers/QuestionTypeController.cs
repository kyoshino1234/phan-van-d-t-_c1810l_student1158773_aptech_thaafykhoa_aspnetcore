﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phone_Project.Contracts;
using Phone_Project.Data;

namespace Phone_Project.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class QuestionTypeController : Controller
    {
        private readonly IQuestionRepository _repo;
        private readonly IQuestionTypeRepository _repoType;
        private readonly IMapper _mapper;
        // GET: QuestionController
        public QuestionTypeController(IQuestionRepository repo, IQuestionTypeRepository repoType, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
            _repoType = repoType;
        }
        public ActionResult Index()
        {
            var Questions = _repoType.FindAll().ToList();

            var model = Questions;
            return View(model);
        }

        // GET: QuestionController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: QuestionController/Create
        [Route("Create")]
        public ActionResult Create()
        {
            ICollection<QuestionType> listQT = _repoType.FindAll().ToList();
            ViewBag.QuestionType = listQT;

            return View();

        }

        // POST: QuestionController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("CreateQuestion")]
        public ActionResult Create(QuestionType model)
        {
            try
            {
                Boolean fag = false;
                if (model.NameQType != null)
                {
                    if (!ModelState.IsValid)
                    {
                        return View(model);
                    }
                    //var Question = _mapper.Map<QuestionVM,Question>(model);
                    //Question.DateCreated = new DateTime();
                    var Question = model;
                    Question.DateCreated = new DateTime();
                    Question.Status = 1;
                    var isSuccess = _repoType.Create(Question);
                    if (!isSuccess)
                    {
                        ModelState.AddModelError("", "Fail Create Question");
                        return View(model);
                    }
                    fag = true;
                }
                if (fag == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View();
                }

            }
            catch
            {
                return View();
            }
        }

        // GET: QuestionController/Edit/5
        [Route("Edit")]
        public ActionResult Edit(int id)
        {
            if (!_repo.isExists(id))
            {
                return NotFound();
            }
            QuestionType Question = null;
            Question = _repoType.FindById(id);
            var model = Question;
            return View(model);

        }

        // POST: QuestionController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("EditQuestion")]
        public ActionResult Edit(QuestionType model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                model.DateCreated = new DateTime();
                var isSuccess = _repoType.Update(model);
                if (!isSuccess)
                {
                    ModelState.AddModelError("", "Fail Create Question");
                    return View(model);
                }
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: QuestionController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: QuestionController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
