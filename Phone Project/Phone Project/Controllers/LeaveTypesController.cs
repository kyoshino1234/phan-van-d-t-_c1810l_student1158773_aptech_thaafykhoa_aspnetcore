﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phone_Project.Contracts;
using Phone_Project.Data;
using Phone_Project.Models;

namespace Phone_Project.Controllers
{
    public class LeaveTypesController : Controller
    {
        private readonly ILeaveTypeRepository _repo;
        private readonly IMapper _mapper;
        // GET: LeaveTypeController
        public LeaveTypesController(ILeaveTypeRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        public ActionResult Index()
        {
            var leaveTypes = _repo.FindAll().ToList();
            //var model = _mapper.Map<List<LeaveType>, List<LeaveTypeVM>>(leaveTypes);
            //var model = _mapper.Map<List<LeaveType>, List<LeaveTypeVM>>(leaveTypes);
            var model = leaveTypes;
            return View(model);
        }

        // GET: LeaveTypeController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LeaveTypeController/Create
        public ActionResult Create()
        {
            return View();
           
        }

        // POST: LeaveTypeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LeaveType model)
        {
            try
            {
                Boolean fag = false;
                if (model.Name != null)
                {
                    if (!ModelState.IsValid)
                    {
                        return View(model);
                    }
                    //var leaveType = _mapper.Map<LeaveTypeVM,LeaveType>(model);
                    //leaveType.DateCreated = new DateTime();
                    var leaveType = model;
                    leaveType.DateCreated = new DateTime();
                    var isSuccess = _repo.Create(leaveType);
                    if (!isSuccess)
                    {
                        ModelState.AddModelError("", "Fail Create LeaveType");
                        return View(model);
                    }
                    fag = true;
                }
                if (fag == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View();
                }

            }
            catch
            {
                return View();
            }
        }

        // GET: LeaveTypeController/Edit/5
        public ActionResult Edit(int id)
        {
            if(!_repo.isExists(id))
            {
                return NotFound();
            }
            LeaveType  leaveType = null;
            leaveType = _repo.FindById(id);
            var model = leaveType;
            return View(model);

        }

        // POST: LeaveTypeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LeaveType model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                model.DateCreated = new DateTime();
                var isSuccess = _repo.Update(model);
                if (!isSuccess)
                {
                    ModelState.AddModelError("", "Fail Create LeaveType");
                    return View(model);
                }
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LeaveTypeController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LeaveTypeController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
