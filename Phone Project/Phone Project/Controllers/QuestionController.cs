﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Phone_Project.Contracts;
using Phone_Project.Data;

namespace Phone_Project.Controllers
{
    public class QuestionController : Controller
    {
        // GET: QuestionController1
        private readonly IQuestionRepository _repo;
        private readonly IQuestionTypeRepository _repoType;
        private readonly IMapper _mapper;
        // GET: LeaveTypeController
        public QuestionController(IQuestionRepository repo, IMapper mapper, IQuestionTypeRepository repoType)
        {
            _repo = repo;
            _mapper = mapper;
            _repoType = repoType;
        }
        public ActionResult Index()
        {
            var leaveTypes = _repo.FindAll().ToList();
            //var model = _mapper.Map<List<LeaveType>, List<LeaveTypeVM>>(leaveTypes);
            //var model = _mapper.Map<List<LeaveType>, List<LeaveTypeVM>>(leaveTypes);
            foreach(Question item in leaveTypes)
            {
                QuestionType qT = _repoType.FindById(item.IdQType);
                item.QuestionType = qT;
            }
            var model = leaveTypes;
            return View(model);
        }

        /*// GET: QuestionController1/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: QuestionController1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QuestionController1/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: QuestionController1/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: QuestionController1/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: QuestionController1/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: QuestionController1/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }*/
    }
}
