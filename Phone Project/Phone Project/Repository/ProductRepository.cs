﻿using Phone_Project.Contracts;
using Phone_Project.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _db;

        public ProductRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public bool Create(Product entity)
        {
            _db.Products.Add(entity);
            return Save();
        }

        public bool Delete(Product entity)
        {
            _db.Products.Remove(entity);
            return Save();
        }

        public ICollection<Product> FindAll()
        {
            var listProduct = _db.Products.ToList();
            return listProduct;
        }

        public Product FindById(int id)
        {
            var item = _db.Products.Find(id);
            return item;
        }

        public Boolean isExists(int id)
        {
            var exists = _db.Products.Any(item => item.IdProduct == id);
            return exists;
        }
       

        public bool Save()
        {
            var result = _db.SaveChanges();
            return result > 0;
        }

        public bool Update(Product entity)
        {
            _db.Products.Update(entity);
            return Save();
        }
    }
}
