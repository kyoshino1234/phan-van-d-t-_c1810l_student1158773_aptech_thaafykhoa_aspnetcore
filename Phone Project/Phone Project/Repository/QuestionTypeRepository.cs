﻿using Phone_Project.Contracts;
using Phone_Project.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Repository
{
    public class QuestionTypeRepository : IQuestionTypeRepository
    {
         private readonly ApplicationDbContext _db;

        public QuestionTypeRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public bool Create(QuestionType entity)
        {
            _db.QuestionTypes.Add(entity);
            return Save();
        }

        public bool Delete(QuestionType entity)
        {
            _db.QuestionTypes.Remove(entity);
            return Save();
        }

        public ICollection<QuestionType> FindAll()
        {
            var listQuestionType = _db.QuestionTypes.ToList();
            return listQuestionType;
        }

        public QuestionType FindById(int id)
        {
            var item = _db.QuestionTypes.Find(id);
            return item;
        }

        public Boolean isExists(int id)
        {
            var exists = _db.QuestionTypes.Any(item => item.IdQType == id);
            return exists;
        }


        public bool Save()
        {
            var result = _db.SaveChanges();
            return result > 0;
        }

        public bool Update(QuestionType entity)
        {
            _db.QuestionTypes.Update(entity);
            return Save();
        }
    }
}
