﻿using Phone_Project.Contracts;
using Phone_Project.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Repository
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly ApplicationDbContext _db;

        public QuestionRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public bool Create(Question entity)
        {
            _db.Questions.Add(entity);
            return Save();
        }

        public bool Delete(Question entity)
        {
            _db.Questions.Remove(entity);
            return Save();
        }

        public ICollection<Question> FindAll()
        {
            var listQuestion = _db.Questions.ToList();
            return listQuestion;
        }

        public Question FindById(int id)
        {
            var item = _db.Questions.Find(id);
            return item;
        }

        public Boolean isExists(int id)
        {
            var exists = _db.Questions.Any(item => item.Id == id);
            return exists;
        }


        public bool Save()
        {
            var result = _db.SaveChanges();
            return result > 0;
        }

        public bool Update(Question entity)
        {
            _db.Questions.Update(entity);
            return Save();
        }
    }
}
