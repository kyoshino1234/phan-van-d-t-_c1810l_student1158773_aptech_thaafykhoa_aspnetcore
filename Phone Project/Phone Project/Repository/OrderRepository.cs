﻿using Phone_Project.Contracts;
using Phone_Project.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ApplicationDbContext _db;

        public OrderRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public bool Create(Order entity)
        {
            _db.Orders.Add(entity);
            return Save();
        }

        public bool Delete(Order entity)
        {
            _db.Orders.Remove(entity);
            return Save();
        }

        public ICollection<Order> FindAll()
        {
            var listOrder = _db.Orders.ToList();
            return listOrder;
        }

        public Order FindById(int id)
        {
            var item = _db.Orders.Find(id);
            return item;
        }

        public Boolean isExists(int id)
        {
            var exists = _db.Orders.Any(item => item.IdOrder == id);
            return exists;
        }
       

        public bool Save()
        {
            var result = _db.SaveChanges();
            return result > 0;
        }

        public bool Update(Order entity)
        {
            _db.Orders.Update(entity);
            return Save();
        }
    }
}
