﻿using Phone_Project.Contracts;
using Phone_Project.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Repository
{
    public class OrderDetailRepository : IOrderDetailRepository
    {
        private readonly ApplicationDbContext _db;
       

        public OrderDetailRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public bool Create(OrderDetail entity)
        {
            _db.OrderDetails.Add(entity);
            return Save();
        }

        public bool Delete(OrderDetail entity)
        {
            _db.OrderDetails.Remove(entity);
            return Save();
        }

        public ICollection<OrderDetail> FindAll()
        {
            var listOrderDetail = _db.OrderDetails.ToList();
            foreach(var item in listOrderDetail)
            {
                Product product = _db.Products.Find(item.IdProduct);
                item.Product = product;
            }
           
            return listOrderDetail;
        }

        public OrderDetail FindById(int id)
        {
            var item = _db.OrderDetails.Find(id);
            return item;
        }

        public Boolean isExists(int id)
        {
            var exists = _db.OrderDetails.Any(item => item.IdOrderDetail == id);
            return exists;
        }
       

        public bool Save()
        {
            var result = _db.SaveChanges();
            return result > 0;
        }

        public bool Update(OrderDetail entity)
        {
            _db.OrderDetails.Update(entity);
            return Save();
        }

    }
}
