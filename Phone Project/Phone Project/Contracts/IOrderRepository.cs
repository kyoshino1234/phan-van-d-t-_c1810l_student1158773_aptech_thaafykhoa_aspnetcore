﻿using Phone_Project.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Contracts
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
        Boolean isExists(int id);
    }
}
