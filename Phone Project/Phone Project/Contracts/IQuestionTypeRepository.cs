﻿using Phone_Project.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phone_Project.Contracts
{
    public interface IQuestionTypeRepository : IRepositoryBase<QuestionType>
    {
        Boolean isExists(int id);
    }
}
